package com.example.metapi.dto
import android.media.MediaPlayer
import com.google.gson.annotations.SerializedName
import java.util.*

// objekt som lagrer forecasten.
data class AirQualityForecast(
    @SerializedName("meta")
    val meta: Meta? = null,
    @SerializedName("data")
    val data: Data? = null
)

// Objekt som lagrer sted. Ligger i AirQualityForecast-objektet.
data class Meta (
    @SerializedName("location")
    var location : Location,
    @SerializedName("reftime")
    var reftime: String,
    @SerializedName("superlocation")
    var superlocationDTO : Superlocation,
    @SerializedName("sublocation")
    var sublocation: Sublocation
)

// objekt som lagrer liste over målinger. Ligger i AirQualityForecast-objektet.
data class Data(
    @SerializedName("time")
    val time: ArrayList<TimeObject?>? = null
)

// objekt som lagrer sted.
data class Location (
    @SerializedName("areacode")
    var areacode : String,
    @SerializedName("name")
    var name : String,
    @SerializedName("latitude")
    var latitude : String,
    @SerializedName("longitude")
    var longitude : String
)


// objekt som lagrer variabler for en måling.
data class TimeObject(
    @SerializedName("from")
    val from: Date? = null,
    @SerializedName("to")
    val to: Date? = null,
    @SerializedName("variables")
    val variables: Variables? = null
)


// Objekt som lagrer verdiene vi har bestemt er viktige for målingen (vi bare sier dette i rapporten hehe)
data class Variables (
    @SerializedName("pm10_concentration")
    val pm10_concentration: ForecastData?,
    @SerializedName("pm25_concentration")
    val pm25_concentration: ForecastData?,
    @SerializedName("no2_concentration")
    val no2_concentration: ForecastData?,
    @SerializedName("o3_concentration")
    val o3_concentration: ForecastData?,
    @SerializedName("AQI")
    val AQI: ForecastData?
)

data class ForecastData(
    @SerializedName("units")
    val units: String? = null,
    @SerializedName("value")
    val value: Double? = null
) {
    override fun toString(): String = "$value $units"
}

// Trengs kanskje ikke?
class Sublocation()

// Objekt som lagrer overordnet område for måling.
data class Superlocation (var areacode : String, var areaclass : String,
                             var superareacode : String, var name : String,
                             var latitude : String, var longitude : String)

data class CurrentLocation(var latitude: Double, var longitude: Double)
