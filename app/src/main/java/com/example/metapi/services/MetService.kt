package com.example.metapi.services

import android.util.Log
import com.example.metapi.dto.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MetService {

    @GET("weatherapi/airqualityforecast/0.1/")
    fun getForecast(@Query("station") stationEOI: String):
            Call<AirQualityForecast>

    @GET("weatherapi/airqualityforecast/0.1")
    fun getLocationForecast(@Query("lat") lat: String, @Query("lon")lng: String):
            Call<AirQualityForecast>

    companion object {
        const val BASE_URL = "https://in2000-apiproxy.ifi.uio.no/"
        operator fun invoke(): MetService {
            Log.i("MetService", "Companion object invoked")
            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MetService::class.java)
        }
    }
}
