package com.example.metapi.activites_and_fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.metapi.R

class FragmentOm : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_om, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var sidetittel = getString(R.string.Om)
        (activity as BaseActivity).setToolbarTittel(sidetittel)
    }

}
