package com.example.metapi.activites_and_fragments

//Kilde recyclerView: https://codinginflow.com/tutorials/android/recyclerview-cardview/part-2-adapter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.metapi.R
import com.example.metapi.dto.AirQualityForecast
import com.example.metapi.dto.TimeObject
import com.example.metapi.services.MetService
import com.example.metapi.services.RetrofitClientInstance
import kotlinx.android.synthetic.main.fragment_city_forecast.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ForecastFragment : Fragment() {

    private lateinit var myRecyclerView: RecyclerView
    private lateinit var myAdapter: RecyclerView.Adapter<*>
    private lateinit var myManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        return inflater.inflate(R.layout.fragment_city_forecast, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setAPI(ValueHandler().getCityNumber())
    }

    //metode som kaller på ValueHandler-et
    private fun setAPI(stationEOI: String) {

        val service =  RetrofitClientInstance.retrofitInstance?.create(MetService::class.java)
        val call = service?.getForecast(stationEOI)
        call?.enqueue(object : Callback<AirQualityForecast> {
            override fun onFailure(call: Call<AirQualityForecast>, t: Throwable) {
                Log.e ("Json Error", t.toString())
                Toast.makeText(context, "Json Error. ", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<AirQualityForecast>, response: Response<AirQualityForecast>) {
                if (response.body() == null) {
                    Toast.makeText(context, "Response body er null. ", Toast.LENGTH_LONG).show()
                }
                val body: AirQualityForecast? = response.body()
                val meta = body?.meta
                val data = body?.data

                val timeArray: ArrayList<TimeObject?>? = data?.time

                //Ser på forecasten, i intervaller/grupper på 6 timer hver.
                setRecyclerViewGruppert(timeArray)
            }
        })
    }

    // Regner ut og viser time for time-varsel av luftkvaliteten i valgt by
    private fun setRecyclerViewTimeForTime(timeArray: ArrayList<TimeObject?>?) {

        //Legger inn alt vi skal ha fra timeArray inn i en mutableList
        val liste = FormatRecyclerViewCards().setTimeForTime(timeArray)

        //LA STÅ
        this.myManager = LinearLayoutManager(this.context)
        this.myAdapter = ListAdapter(liste)

        this.myRecyclerView = requireActivity().findViewById<RecyclerView>(R.id.recyclerView).apply {

            this.layoutManager = myManager
            this.adapter = myAdapter
            myAdapter.notifyDataSetChanged()
        }

        //Må først sette så recyclerView og knapp synes når man kommer inn.
        recyclerView.setVisibility(View.VISIBLE)
        seMer.setVisibility(View.VISIBLE)
        seMer.text = "Se mindre"


        //Setter knappen.
        val setGrupperingButton = requireActivity().findViewById<Button>(R.id.seMer)
        setGrupperingButton.setOnClickListener{
            if(recyclerView.visibility == 0){
                recyclerView.visibility = View.GONE
            }else{
                recyclerView.visibility = View.VISIBLE
            }
            setRecyclerViewGruppert(timeArray)
        }
    }


    // Regner ut og viser intervall-varsel av luftkvaliteten i valgt by
    private fun setRecyclerViewGruppert(timeArray: ArrayList<TimeObject?>?) {

        seMer.visibility = View.GONE

        //Legger inn alt vi skal ha fra timeArray inn i en mutableList
        val liste2 = FormatRecyclerViewCards().setGruppert(timeArray)

        //LA STÅ
        this.myManager = LinearLayoutManager(this.context)
        this.myAdapter = ListAdapter(liste2)

        this.myRecyclerView = requireActivity().findViewById<RecyclerView>(R.id.recyclerView2).apply {
            this.layoutManager = myManager
            this.adapter = myAdapter
            myAdapter.notifyDataSetChanged()
        }

        //Må først sette så recyclerView og knapp synes når man kommer inn.
        recyclerView2.setVisibility(View.VISIBLE)
        seMer.setVisibility(View.VISIBLE)
        seMer.text = "Time for time"


        //Setter knappen.
        val setTimeForTimeButton = requireActivity().findViewById<Button>(R.id.seMer)
        setTimeForTimeButton.setOnClickListener{

            if(recyclerView2.visibility == 0){
                recyclerView2.visibility = View.GONE
            } else{
                recyclerView2.visibility = View.VISIBLE
            }

            setRecyclerViewTimeForTime(timeArray)

        }
    }
}