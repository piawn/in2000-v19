package com.example.metapi.activites_and_fragments


//Klasse for håndtering av ValueHandler-kall og utregning av verider.
class ValueHandler {

    //representerer EOI-nummeret t
    // il den aktuelle byen
    companion object {
        var cityNumber = ""
    }

    //metode som gir ut EOI-koden til den aktuelle byen.
    fun getCityNumber(): String {
        return cityNumber
    }

    //funksjon som setter nummber på by etter navn på by
    fun setCityNumber(cityName: String) {
        if (cityName == "Oslo"){
            cityNumber = "NO0088A"
        } else if (cityName == "Bergen"){
            cityNumber = "NO0120A"
        } else if (cityName == "Stavanger"){
            cityNumber = "NO0076A"
        } else if (cityName == "Trondheim"){
            cityNumber = "NO0089A"
        } else if (cityName == "Tromsø"){
            cityNumber = "NO0079A"
        }
    }

    //Metode som setter navn på by etter EOI-kode, fra ValueHandler-kall
    fun getCityName() : String{
        if (cityNumber == "NO0088A"){
            return "oslo"
        } else if (cityNumber == "NO0120A"){
            return "bergen"
        } else if (cityNumber == "NO0076A"){
            return "stavanger"
        } else if (cityNumber == "NO0089A"){
            return "trondheim"
        } else if (cityNumber == "NO0079A"){
            return "tromsø"
        } else {
            return "faen"
        }
    }

    //Gir ut nummer som representerere et luftforurensingsintervall, basert på streng den tar inn.
    fun findNumber(verdi: String): Int{
        if (verdi == "lite"){
            return 1
        } else if (verdi == "moderat"){
            return 2
        } else if (verdi == "høy"){
            return 3
        } else if (verdi == "svært høy"){
            return 4
        } else {
            return 0
        }
    }

    //Gir ut streng som representerere et luftforurensingsintervall, basert på nummer den tar inn.
    fun findValue(number: Int): String{
        if (number == 1){
            return "lite"
        } else if (number == 2){
            return "moderat"
        } else if (number == 3){
            return "høy"
        } else if (number == 4){
            return "svært høy"
        } else {
            return "ingen data"
        }
    }

    fun findSuperLocation(number: String): String{
        if (number == "0301") {
            return "Oslo"
        } else if (number == "5001"){
            return "Trondheim"
        } else if (number == "1902"){
            return "Tromsø"
        } else if (number == "1201"){
            return "Bergen"
        } else if (number == "1103"){
            return "Stavanger"
        } else {
            return "ingen data"
        }
    }

    fun getCategory(aqi: Double?): String{
        if (aqi != null && aqi >= 0){
            if (aqi < 2){
                return "lite"
            } else if (aqi < 3){
                return "moderat"
            } else if (aqi < 4){
                return "høy"
            } else if (aqi < 5){
                return "svært høy"
            }
        }
        return "feil"
    }

    fun getCategoryNumber(aqi: Double?): Int{
        if (aqi != null && aqi >= 0){
            if (aqi < 2){
                return 1
            } else if (aqi < 3){
                return 2
            } else if (aqi < 4){
                return 3
            } else if (aqi < 5){
                return 4
            }
        }
        return 0
    }

}