package com.example.metapi.activites_and_fragments

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.metapi.R
import kotlinx.android.synthetic.main.activity_config.*

class ConfigActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)
        setImageButtons()
    }

    // Lager imagebuttons som sender videre til valgt fragment
    fun setImageButtons() {
        position_imgbtn.setOnClickListener {
            StartValues.viewPagerType = 1
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)
        }

        stavanger_imgbtn.setOnClickListener {
            //Setter BaseActivity sin cityNumber er EOI-koden for sentrum i Stavanger.
            ValueHandler().setCityNumber("Stavanger")
            StartValues.viewPagerType = 2
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)

        }

        oslo_imgbtn.setOnClickListener {
            //Setter BaseActivity sin cityNumber er EOI-koden for sentrum i Oslo.
            ValueHandler().setCityNumber("Oslo")
            StartValues.viewPagerType = 2
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)
        }

        tromso_imgbtn.setOnClickListener {
            //Setter BaseActivity sin cityNumber er EOI-koden for sentrum i Tromsø .
            ValueHandler().setCityNumber("Tromsø")
            StartValues.viewPagerType = 2
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)
        }

        trondheim_imgbtn.setOnClickListener {
            //Setter BaseActivity sin cityNumber er EOI-koden for sentrum i Trondheim
            ValueHandler().setCityNumber("Trondheim")
            StartValues.viewPagerType = 2
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)
        }

        bergen_imgbtn.setOnClickListener {
            //Setter BaseActivity sin cityNumber er EOI-koden for sentrum i Bergen.
            ValueHandler().setCityNumber("Bergen")
            StartValues.viewPagerType = 2
            val intent = Intent(this, BaseActivity::class.java)
            startActivity(intent)
        }
    }


}
