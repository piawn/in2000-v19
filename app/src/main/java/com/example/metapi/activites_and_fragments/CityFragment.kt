package com.example.metapi.activites_and_fragments

import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.metapi.R
import com.example.metapi.dto.*
import com.example.metapi.services.MetService
import com.example.metapi.services.RetrofitClientInstance
import kotlinx.android.synthetic.main.fragment_city.*
import kotlin.collections.ArrayList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CityFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sidetittel = "By"
        (activity as BaseActivity).setToolbarTittel(sidetittel)
        fabInfo()

        val cityNumber = ValueHandler().getCityNumber()
        setAPI(cityNumber)
    }

    // Sender videre til informasjonssiden
    fun fabInfo() {
        fabInfo.setOnClickListener {
            val intent = Intent(this@CityFragment.context, InformationActivity::class.java)
            startActivity(intent)
        }
    }


    //metode som foreløpig setter station til å være location.name, altså Grønland.
    private fun setAPI(stationEOI: String) {
        val service =  RetrofitClientInstance.retrofitInstance?.create(MetService::class.java)
        val call = service?.getForecast(stationEOI)
        call?.enqueue(object : Callback<AirQualityForecast> {
            override fun onFailure(call: Call<AirQualityForecast>, t: Throwable) {
                Log.e ("Json Error", t.toString())
                Toast.makeText(context, "Json Error. ", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<AirQualityForecast>, response: Response<AirQualityForecast>) {
                if (response.body() == null) {
                    Toast.makeText(context, "Response body er null. ", Toast.LENGTH_LONG).show()
                }
                val body: AirQualityForecast? = response.body()
                val meta = body?.meta
                val data= body?.data

                val timeArray: ArrayList<TimeObject?>? = data?.time
                val delomraade: String? = meta?.location?.name

                //henter ut nåværende tidspunkt i int.
                val time = Time().getTime()

                val hentLocation = ValueHandler().getCityName()

                textViewCity.text = hentLocation.toUpperCase()
                tv_location.text = delomraade?.toUpperCase()
                reftime_txt.text = "Varsling for kl. ${time}"


                val aqi = timeArray?.get(time)?.variables?.AQI?.value

                var text = ValueHandler().getCategory(aqi)

                if (text == "lite") {
                    text = text.toUpperCase()
                    tv_airMessage.text = text
                    tv_airMessage.setTextColor(resources.getColor(R.color.colorLite))
                    val nr = Marker().calculateMarkerNumberLite(aqi)
                    setMarker(nr)
                } else if (text == "moderat") {
                    text = text.toUpperCase()
                    tv_airMessage.text = text
                    tv_airMessage.setTextColor(resources.getColor(R.color.colorModerat))
                    val nr = Marker().calculateMarkerNumberModerat(aqi)
                    setMarker(nr)
                } else if (text == "høy") {
                    text = text.toUpperCase()
                    tv_airMessage.text = text
                    tv_airMessage.setTextColor(resources.getColor(R.color.colorHoyt))
                    val nr = Marker().calculateMarkerNumberHoy(aqi)
                    setMarker(nr)
                } else if (text == "svært høy") {
                    text = text.toUpperCase()
                    tv_airMessage.text = text
                    tv_airMessage.setTextColor(resources.getColor(R.color.colorSvaertHoyt))
                    val nr = Marker().calculateMarkerNumberSvartHoy(aqi)
                    setMarker(nr)

                }
            }
        })
    }

    // Setter markøren på luftkvalitetsskalaen
    private fun setMarker(bias: Float) {
        val set = ConstraintSet()
        set.clone(context, R.layout.fragment_city)
        set.setHorizontalBias(R.id.skala_marker, bias)
        set.applyTo(layout_airMessage)
    }

}