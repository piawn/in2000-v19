package com.example.metapi.activites_and_fragments

//Kilder recyclerView: https://codinginflow.com/tutorials/android/recyclerview-cardview/part-2-adapter

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.metapi.R
import com.example.metapi.dto.AirQualityForecast
import com.example.metapi.dto.Data
import com.example.metapi.dto.Meta
import com.example.metapi.dto.TimeObject
import com.example.metapi.services.MetService
import com.example.metapi.services.RetrofitClientInstance
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.fragment_postition_forecast.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ForecastPositionFragment : Fragment() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var currentLat: Double? = null
    private var currentLon: Double? = null

    private val MY_PERMISSION_CODE = 1000

    private lateinit var myRecyclerView: RecyclerView
    private lateinit var myAdapter: RecyclerView.Adapter<*>
    private lateinit var myManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        return inflater.inflate(R.layout.fragment_postition_forecast, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        checkPermission()

        obtieneLocalizacion()
    }

    @SuppressLint("MissingPermission")
    private fun obtieneLocalizacion() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                currentLat =  location?.latitude
                currentLon = location?.longitude
                setPosition(location?.latitude.toString(),location?.longitude.toString())
            }

    }

    private fun setPosition(lat: String, lon: String) {
        val service =  RetrofitClientInstance.retrofitInstance?.create(MetService::class.java)
        val call = service?.getLocationForecast(lat, lon)
        call?.enqueue(object : Callback<AirQualityForecast> {
            override fun onFailure(call: Call<AirQualityForecast>, t: Throwable) {
                Log.e("Json Error", t.toString())
                Toast.makeText(BaseActivity(), "Json Error. ", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<AirQualityForecast>, response: Response<AirQualityForecast>) {
                if (response.body() == null) {
                    Log.i("ValueHandler null", "Can't get values from ValueHandler. ")
                }
                val body: AirQualityForecast? = response.body()
                val meta: Meta? = body?.meta
                val data: Data? = body?.data

                val timeArray: ArrayList<TimeObject?>? = data?.time

                setRecyclerViewGruppert(timeArray)

            }
        })
    }

    //KAN VURDERE Å LEGGE ALT DET UNDER HER I EN EGEN KLASSE.
    private fun checkPermission() {
        //request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                obtieneLocalizacion()
            }

        }
        else {
            obtieneLocalizacion()
        }
    }

    private fun checkLocationPermission(): Boolean  {
        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSION_CODE)
            } else {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION), MY_PERMISSION_CODE)
                return false
            }
        }
        return true
    }

    // Regner ut og viser time for time-varsel av luftkvaliteten i min posisjon
    private fun setRecyclerViewTimeForTime(timeArray: ArrayList<TimeObject?>?) {

        //Legger inn alt vi skal ha fra timeArray inn i en mutableList
        val liste = FormatRecyclerViewCards().setTimeForTime(timeArray)

        //LA STÅ
        myManager = LinearLayoutManager(this.context)
        myAdapter = ListAdapter(liste)

        myRecyclerView = requireActivity().findViewById<RecyclerView>(R.id.recyclerViewTFT).apply {

            this.layoutManager = myManager
            this.adapter = myAdapter
            myAdapter.notifyDataSetChanged()
        }

        //Må først sette så recyclerView og knapp synes når man kommer inn.
        recyclerViewTFT.visibility = View.VISIBLE
        seMerPos.visibility = View.VISIBLE
        seMerPos.text = "Se mindre"


        //Setter knappen.
        val setGrupperingButton = requireActivity().findViewById<Button>(R.id.seMerPos)
        setGrupperingButton.setOnClickListener{

            if(recyclerViewTFT.visibility == 0){
                recyclerViewTFT.visibility = View.GONE
            }else{
                recyclerViewTFT.visibility = View.VISIBLE
            }

            setRecyclerViewGruppert(timeArray)

        }
    }

    // Regner ut og viser intervall-varsel av luftkvaliteten i min posisjon
    private fun setRecyclerViewGruppert(timeArray: ArrayList<TimeObject?>?) {

        seMerPos.setVisibility(View.GONE)

        //Legger inn alt vi skal ha fra timeArray inn i en mutableList
        val liste2 = FormatRecyclerViewCards().setGruppert(timeArray)

        //LA STÅ
        myManager = LinearLayoutManager(this.context)
        myAdapter = ListAdapter(liste2)

        myRecyclerView = requireActivity().findViewById<RecyclerView>(R.id.recyclerViewIntervall).apply {

            this.layoutManager = myManager
            this.adapter = myAdapter
            myAdapter.notifyDataSetChanged()
        }

        //Må først sette så recyclerView og knapp synes når man kommer inn.
        recyclerViewIntervall.setVisibility(View.VISIBLE)
        seMerPos.setVisibility(View.VISIBLE)
        seMerPos.text = "Time for time"


        //Setter knappen.
        val setTimeForTimeButton = requireActivity().findViewById<Button>(R.id.seMerPos)
        setTimeForTimeButton.setOnClickListener{

            if(recyclerViewIntervall.visibility == 0){
                recyclerViewIntervall.setVisibility(View.GONE)
            } else{
                recyclerViewIntervall.setVisibility(View.VISIBLE)
            }

            setRecyclerViewTimeForTime(timeArray)

        }
    }
}