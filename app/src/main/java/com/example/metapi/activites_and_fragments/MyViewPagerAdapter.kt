package com.example.metapi.activites_and_fragments

//Kilde: https://www.youtube.com/watch?v=SHFM3Qw43LE


import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager){
    private val fragmentList : MutableList<Fragment> = ArrayList()
    private val titleList : MutableList<String> = ArrayList()

    override fun getItem(p0: Int): Fragment {
        return fragmentList[p0]
    }
    override fun getCount(): Int {
        return fragmentList.size
    }
    fun addFragment(fragment : Fragment, title : String){
        fragmentList.add(fragment)
        titleList.add(title)
    }
    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }
}