package com.example.metapi.activites_and_fragments

import android.support.v7.widget.CardView
import android.view.LayoutInflater
import com.example.metapi.R
import kotlinx.android.synthetic.main.element.*

class PrognoseElement(private var klokkeslett: String, private var kvalitetsmelding: String, private var color: Int) {

    fun getKlokkeslett(): String{return klokkeslett}
    fun getKvalitetsmelding() : String{return kvalitetsmelding}
    fun getColor() : Int{return color}
}