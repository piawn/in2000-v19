package com.example.metapi.activites_and_fragments

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button


class InformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.metapi.R.layout.activity_information)

        val exitInfo = findViewById<FloatingActionButton>(com.example.metapi.R.id.exitInfoFab)
        exitInfo.setOnClickListener {
            finish()
        }


        val table1 = findViewById<View>(com.example.metapi.R.id.tableLite)
        table1.visibility = View.GONE

        val table2 = findViewById<View>(com.example.metapi.R.id.tableModerat)
        table2.visibility = View.GONE

        val table3= findViewById<View>(com.example.metapi.R.id.tableHoy)
        table3.visibility = View.GONE

        val table4= findViewById<View>(com.example.metapi.R.id.tableSvaertHoy)
        table4.visibility = View.GONE


        val btnShowLite = findViewById<Button>(com.example.metapi.R.id.btnLite)
        btnShowLite.setOnClickListener{
            closeButtons(1)

            if(table1.visibility == 0){
                table1.visibility = View.GONE
            }
            else{
                table1.visibility = View.VISIBLE
            }
        }

        val btnShowModerat = findViewById<Button>(com.example.metapi.R.id.btnModerat)
        btnShowModerat.setOnClickListener{
            closeButtons(2)

            if(table2.visibility == 0){
                table2.visibility = View.GONE
            }
            else{
                table2.visibility = View.VISIBLE
            }
        }

        val btnShowHoy = findViewById<Button>(com.example.metapi.R.id.btnHoy)
        btnShowHoy.setOnClickListener{
            closeButtons(3)

            if(table3.visibility == 0){
                table3.visibility = View.GONE
            }
            else{
                table3.visibility = View.VISIBLE
            }
        }

        val btnShowSvaertHoy = findViewById<Button>(com.example.metapi.R.id.btnSvaertHoy)
        btnShowSvaertHoy.setOnClickListener{
            closeButtons(4)

            if(table4.visibility == 0){
                table4.visibility = View.GONE
            }
            else{
                table4.visibility = View.VISIBLE
            }
        }
    }

    fun closeButtons(nr: Int){

        val table1 = findViewById<View>(com.example.metapi.R.id.tableLite)
        val table2 = findViewById<View>(com.example.metapi.R.id.tableModerat)
        val table3= findViewById<View>(com.example.metapi.R.id.tableHoy)
        val table4= findViewById<View>(com.example.metapi.R.id.tableSvaertHoy)

        if (nr == 1){
            table2.visibility = View.GONE
            table3.visibility = View.GONE
            table4.visibility = View.GONE
        } else if (nr == 2){
            table1.visibility = View.GONE
            table3.visibility = View.GONE
            table4.visibility = View.GONE
        } else if (nr == 3){
            table1.visibility = View.GONE
            table2.visibility = View.GONE
            table4.visibility = View.GONE
        } else {
            table1.visibility = View.GONE
            table2.visibility = View.GONE
            table3.visibility = View.GONE
        }

    }
}