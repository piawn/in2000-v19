package com.example.metapi.activites_and_fragments

import com.example.metapi.dto.TimeObject
import java.util.ArrayList

class FormatRecyclerViewCards {

    //Setter time for time i prognosefragmentene
    fun setTimeForTime(timeArray: ArrayList<TimeObject?>?): MutableList<PrognoseElement>{
        //Oppretter liste over cardViews
        val liste = mutableListOf<PrognoseElement>()
        //Setter første "overskrift", som er I dag.
        liste.add(PrognoseElement("I dag", "",0))

        val time = Time().getTime()
        var diff = 24 - time
        var teller = 0

        //Setter kort for resterende timer ila dagen
        while (diff > 0){

            val tid = time + teller

            val aqi = timeArray?.get(tid)?.variables?.AQI?.value

            val utString = ValueHandler().getCategory(aqi)
            val utVerdi = ValueHandler().findNumber(utString)

            if (tid < 10){
                liste.add(PrognoseElement("kl. 0${tid}", utString.toUpperCase(), utVerdi))
            } else {
                liste.add(PrognoseElement("kl. ${tid}", utString.toUpperCase(), utVerdi))
            }

            teller += 1
            diff -= 1
        }


        //Setter andre"overskrift", som er I morgen.
        liste.add(PrognoseElement("I morgen", "", 0))

        var nedteller = 0

        while (nedteller < 24) {

            val aqi = timeArray?.get(24+nedteller)?.variables?.AQI?.value

            val utString = ValueHandler().getCategory(aqi)
            val utVerdi = ValueHandler().findNumber(utString)

            if (nedteller < 10){
                liste.add(PrognoseElement("kl. 0${nedteller}", utString.toUpperCase(), utVerdi))
            } else {
                liste.add(PrognoseElement("kl. ${nedteller}", utString.toUpperCase(), utVerdi))
            }

            nedteller += 1
        }

        return liste
    }

    //Sette sekstimersintervallene i forecast
    fun setGruppert(timeArray: ArrayList<TimeObject?>?): MutableList<PrognoseElement>{

        //Oppretter liste nr 2 over cardviews, som opprettes i recycleView nr 2
        val liste2 = mutableListOf<PrognoseElement>()
        //Setter første "overskrift", som er I dag.
        liste2.add(PrognoseElement("I dag", "", 0))

        val startTime = Time().getTime()
        var time = startTime
        var teller = time


        //Går gjennom intervall for intervall, for samme dag.
        var firstIntervalBiggest = 0
        while (time < 6){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > firstIntervalBiggest){
                firstIntervalBiggest = value
            }
            time++
        }

        if (firstIntervalBiggest != 0){
            val utStreng = ValueHandler().findValue(firstIntervalBiggest)
            liste2.add(PrognoseElement("kl. 0${startTime} - 06", utStreng.toUpperCase(), firstIntervalBiggest))
        }

        var secondIntervalBiggest = 0
        while (time >= 6 && time < 12){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > secondIntervalBiggest){
                secondIntervalBiggest = value
            }
            time++
        }

        if (secondIntervalBiggest != 0 && (startTime >= 6 && startTime < 12)){
            val utStreng = ValueHandler().findValue(secondIntervalBiggest)
            if (startTime < 10){
                liste2.add(PrognoseElement("kl. 0${startTime} - 12", utStreng.toUpperCase(), secondIntervalBiggest))
            } else {
                liste2.add(PrognoseElement("kl. ${startTime} - 12", utStreng.toUpperCase(), secondIntervalBiggest))
            }
        }

        if (secondIntervalBiggest != 0 && !(startTime >= 6 && startTime < 12)){
            val utStreng = ValueHandler().findValue(secondIntervalBiggest)
            liste2.add(PrognoseElement("kl. 06 - 12", utStreng.toUpperCase(), secondIntervalBiggest))
        }


        var thirdIntervalBiggest = 0
        while (time >= 12 && time < 18){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > thirdIntervalBiggest){
                thirdIntervalBiggest = value
            }
            time++
        }

        if (thirdIntervalBiggest != 0 && (startTime >= 12 && startTime < 18)){
            val utStreng = ValueHandler().findValue(thirdIntervalBiggest)
            liste2.add(PrognoseElement("kl. ${startTime} - 18", utStreng.toUpperCase(), thirdIntervalBiggest))
        }

        if (thirdIntervalBiggest != 0 && !(startTime >= 12 && startTime < 18)){
            val utStreng = ValueHandler().findValue(thirdIntervalBiggest)
            liste2.add(PrognoseElement("kl. 12 - 18", utStreng.toUpperCase(), thirdIntervalBiggest))
        }




        var fourthIntervalBiggest = 0
        while (time >= 18 && time < 24){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > fourthIntervalBiggest){
                fourthIntervalBiggest = value
            }
            time++
        }

        if (fourthIntervalBiggest != 0 && (startTime >= 18 && startTime < 24)){
            val utStreng = ValueHandler().findValue(fourthIntervalBiggest)
            liste2.add(PrognoseElement("kl. ${startTime} - 24", utStreng.toUpperCase(), fourthIntervalBiggest))
        }

        if (fourthIntervalBiggest != 0 && !(startTime >= 18 && startTime < 24)){
            val utStreng = ValueHandler().findValue(fourthIntervalBiggest)
            liste2.add(PrognoseElement("kl. 18 - 24", utStreng.toUpperCase(), fourthIntervalBiggest))
        }


        //---------------------- I MORGEN!!! ------------------------

        liste2.add(PrognoseElement("I morgen", "",0))


        firstIntervalBiggest = 0
        while (time < 30){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > firstIntervalBiggest){
                firstIntervalBiggest = value
            }
            time++
        }

        var utStreng = ValueHandler().findValue(firstIntervalBiggest)
        liste2.add(PrognoseElement("kl. 00 - 06", utStreng.toUpperCase(), firstIntervalBiggest))

        secondIntervalBiggest = 0
        while (time >= 30 && time < 36){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > secondIntervalBiggest){
                secondIntervalBiggest = value
            }
            time++
        }

        utStreng = ValueHandler().findValue(secondIntervalBiggest)
        liste2.add(PrognoseElement("kl. 06 - 12", utStreng.toUpperCase(), secondIntervalBiggest))

        thirdIntervalBiggest = 0
        while (time >= 36 && time < 42){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > thirdIntervalBiggest){
                thirdIntervalBiggest = value
            }
            time++
        }

        utStreng = ValueHandler().findValue(thirdIntervalBiggest)
        liste2.add(PrognoseElement("kl. 12 - 18", utStreng.toUpperCase(), thirdIntervalBiggest))

        fourthIntervalBiggest = 0
        while (time >= 42 && time < 48){
            val aqi = timeArray?.get(time)?.variables?.AQI?.value
            val value = ValueHandler().getCategoryNumber(aqi)

            if (value > fourthIntervalBiggest){
                fourthIntervalBiggest = value
            }
            time++
        }

        utStreng = ValueHandler().findValue(fourthIntervalBiggest)
        liste2.add(PrognoseElement("kl. 18 - 24", utStreng.toUpperCase(), fourthIntervalBiggest))

        return liste2

    }
}