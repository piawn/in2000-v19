package com.example.metapi.activites_and_fragments
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.metapi.R
import com.example.metapi.dto.AirQualityForecast
import com.example.metapi.dto.Data
import com.example.metapi.dto.Meta
import com.example.metapi.dto.TimeObject
import com.example.metapi.services.MetService
import com.example.metapi.services.RetrofitClientInstance
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.fragment_city.fabInfo
import kotlinx.android.synthetic.main.fragment_position.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PositionFragment : Fragment() {

    //variabler som trengs for  finne location.
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var currentLat: Double? = null
    private var currentLon: Double? = null
    private val MY_PERMISSION_CODE = 1000

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        (activity as AppCompatActivity).supportActionBar?.show()
        return inflater.inflate(R.layout.fragment_position, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sidetittel = "Min posisjon"
        (activity as BaseActivity).setToolbarTittel(sidetittel)
        fabInfo()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        checkPermission()
    }

    // Sender videre til informasjonssiden
    private fun fabInfo() {
        fabInfo.setOnClickListener {
            val intent = Intent(this@PositionFragment.context, InformationActivity::class.java)
            startActivity(intent)
        }
    }

    //Finner siste location, setter verdier til å være riktige.
    @SuppressLint("MissingPermission")
    private fun obtieneLocalizacion(){
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                currentLat =  location?.latitude
                currentLon = location?.longitude
                setPosition(location?.latitude.toString(),location?.longitude.toString())

                if (fusedLocationClient.lastLocation == null) {
                    Toast.makeText(context, "Tjenesten fungerer kun med stedstjenester. Slå det på i innstillinger. ", Toast.LENGTH_LONG).show()
                }
            }
    }


    //Metode som kaller på ValueHandler-et og setter verdier deretter.
    private fun setPosition(lat: String, lon: String) {
        val service =  RetrofitClientInstance.retrofitInstance?.create(MetService::class.java)
        val call = service?.getLocationForecast(lat, lon)
        call?.enqueue(object : Callback<AirQualityForecast> {
            override fun onFailure(call: Call<AirQualityForecast>, t: Throwable) {
                Log.e("Json Error", t.toString())
                Toast.makeText(BaseActivity(), "Json Error. ", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<AirQualityForecast>, response: Response<AirQualityForecast>) {
                if (response.body() == null) {
                    Log.i("ValueHandler null", "Canto't get values from ValueHandler. ")
                }
                val body: AirQualityForecast? = response?.body()
                val meta: Meta? = body?.meta
                val data: Data? = body?.data

                val station = meta?.superlocationDTO?.areacode
                //Toast.makeText(BaseActivity(), station, Toast.LENGTH_LONG).show()

                val superareacode = station.toString().substring(0..3)

                val superLocation = meta?.superlocationDTO?.name
                val superLocationCity = ValueHandler().findSuperLocation(superareacode)
                val timeArray: ArrayList<TimeObject?>? = data?.time

                //henter ut nåværende tidspunkt i int.
                val time = Time().getTime()

                val aqi = timeArray?.get(time)?.variables?.AQI?.value

                var text = ValueHandler().getCategory(aqi)

                if (text == "ingen data"){
                    Toast.makeText(context, "Siden fungerer kun med stedstjenester. ", Toast.LENGTH_LONG).show()
                }

                tv_pos_location.text = superLocation
                tv_pos_city.text = superLocationCity.toUpperCase()
                pos_reftime_txt.text = "Varsling for kl. ${time}"

                if (text == "lite") {
                    text = text.toUpperCase()
                    tv_pos_airMessage.text = text
                    tv_pos_airMessage.setTextColor(resources.getColor(R.color.colorLite))
                    val nr = Marker().calculateMarkerNumberLite(aqi)
                    setMarkerPosition(nr)
                } else if (text == "moderat") {
                    text = text.toUpperCase()
                    tv_pos_airMessage.text = text
                    tv_pos_airMessage.setTextColor(resources.getColor(R.color.colorModerat))
                    val nr  = Marker().calculateMarkerNumberModerat(aqi)
                    setMarkerPosition(nr)
                } else if (text == "høy") {
                    text = text.toUpperCase()
                    tv_pos_airMessage.text = text
                    tv_pos_airMessage.setTextColor(resources.getColor(R.color.colorHoyt))
                    val nr = Marker().calculateMarkerNumberHoy(aqi)
                    setMarkerPosition(nr)
                } else if (text == "svært høy") {
                    text = text.toUpperCase()
                    tv_pos_airMessage.text = text
                    tv_pos_airMessage.setTextColor(resources.getColor(R.color.colorSvaertHoyt))
                    val nr  = Marker().calculateMarkerNumberSvartHoy(aqi)
                    setMarkerPosition(nr)
                }
            }
        })
    }

    //requester runtime permission
    private fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                obtieneLocalizacion()
            }
            //Toast.makeText(context, "Tjenesten fungerer kun med stedstjenester. Slå det på i innstillinger. ", Toast.LENGTH_LONG).show()
        }
        else {
            obtieneLocalizacion()
            Toast.makeText(context, "Tjenesten fungerer kun med stedstjenester. Slå det på i innstillinger. ", Toast.LENGTH_LONG).show()
        }
    }

    //sjekker etter permission
    private fun checkLocationPermission(): Boolean  {
        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSION_CODE)
            } else {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION), MY_PERMISSION_CODE)
                return false
            }
        }
        return true
    }

    // Setter markøren på luftkvalitetskalaen
    private fun setMarkerPosition(bias: Float) {
        val set = ConstraintSet()
        set.clone(context, R.layout.fragment_position)
        set.setHorizontalBias(R.id.skala_marker_pos, bias)
        set.applyTo(layout_airMessage_pos)
    }

}



