package com.example.metapi.activites_and_fragments

class Marker {

    // Regner ut hvor på skalaen markøren skal settes
    fun calculateMarkerNumberLite(aqi: Double?): Float{

        //Må gå via streng for å få Double? til Double
        val aqiStreng = aqi.toString()
        val aqiDouble = aqiStreng.toDouble()

        val aqiRange = (23.0/2.0)
        val aqiValue = (aqiRange * aqiDouble)

        return (aqiValue/100).toFloat()
    }

    fun calculateMarkerNumberModerat(aqi: Double?): Float{

        //Må gå via streng for å få Double? til Double
        val aqiStreng = aqi.toString()
        val aqiDouble = aqiStreng.toDouble()

        val aqiRange = (50.0/3.0)
        val aqiValue = (aqiRange * aqiDouble)

        return (aqiValue/100).toFloat()
    }

    fun calculateMarkerNumberHoy(aqi: Double?): Float{

        //Må gå via streng for å få Double? til Double
        val aqiStreng = aqi.toString()
        val aqiDouble = aqiStreng.toDouble()

        val aqiRange = (77.0/4.0)
        val aqiValue = (aqiRange * aqiDouble)

        return (aqiValue/100).toFloat()
    }

    fun calculateMarkerNumberSvartHoy(aqi: Double?): Float{

        //Må gå via streng for å få Double? til Double
        val aqiStreng = aqi.toString()
        val aqiDouble = aqiStreng.toDouble()

        val aqiRange = (100.0/5.0)
        val aqiValue = (aqiRange * aqiDouble)

        if (aqiValue.toInt() > 100){
            return 1.toFloat()
        }
        else {
            return (aqiValue/100).toFloat()
        }
    }
}