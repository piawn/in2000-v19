package com.example.metapi.activites_and_fragments

import com.example.metapi.dto.TimeObject
import java.text.SimpleDateFormat
import java.util.*

class Time {

    //Gir ut nåværende tidspunkt i int. Hvis den er mer enn 30 min over hel, viser den neste time
    fun getTime(): Int {
        val currentTime = SimpleDateFormat("HH:mm ")
        val time = currentTime.format(Date())

        if (time.substring(3..4).toInt() > 30) {
            return time.substring(0..1).toInt() + 1
        } else {
            return time.substring(0..1).toInt()
        }
    }
}