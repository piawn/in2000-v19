package com.example.metapi.activites_and_fragments

//Kilde viewPager: https://www.youtube.com/watch?v=SHFM3Qw43LE

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.metapi.*
import kotlinx.android.synthetic.main.activity_base.*


open class BaseActivity : AppCompatActivity() {


    private lateinit var drawerLayout: DrawerLayout
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState : Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        if (StartValues.startHandler == 1){
            showConfig()
            StartValues.startHandler = 2
        } else {
            showViewPager()
        }

        addToolbar()
        addDrawerLayout()
    }


    // Åpner config første gang
    fun showConfig() {
        val intent = Intent(this, ConfigActivity::class.java)
        startActivity(intent)
    }

    //Åpner riktig type viewPager
    fun showViewPager(){
        if (StartValues.viewPagerType == 1){
            createViewPagerPosition()
        } else if (StartValues.viewPagerType == 2){
            createViewPager()
        } else {
            Toast.makeText(this, "Kunne ikke starte ViewPager ", Toast.LENGTH_LONG).show()
        }
    }

    fun setToolbarTittel(tittel: String) {
        toolbar.title = tittel
    }


    // Lager en toolbar og aktiverer den med setSupportActionBar. cityToolbar er definert i city_toolbar.xml
    fun addToolbar(){
        toolbar = findViewById(R.id.cityToolbar)
        setSupportActionBar(toolbar)

        //Setter inn menyknappen
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
        }
    }

    //Setter funksjonalitet på drawerLayout (sidemenyen)
    fun addDrawerLayout() {

        drawerLayout = findViewById(R.id.drawer_layout)

        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener {  menuItem ->
            //sett element som valgt for å få highlight
            menuItem.isChecked = true
            //Lukk drawer når elementet er valgt
            drawerLayout.closeDrawers()

            when(menuItem.itemId) {
                R.id.nav_minPos -> {
                    closeViewPager()
                    createViewPagerPosition()
                }
                R.id.nav_oslo -> {
                    ValueHandler().setCityNumber("Oslo")
                    closeViewPagerPosition()
                    createViewPager()
                }

                R.id.nav_bergen -> {
                    ValueHandler().setCityNumber("Bergen")
                    closeViewPagerPosition()
                    createViewPager()
                }
                R.id.nav_stavanger -> {
                    ValueHandler().setCityNumber("Stavanger")
                    closeViewPagerPosition()
                    createViewPager()
                }
                R.id.nav_trondheim -> {
                    ValueHandler().setCityNumber("Trondheim")
                    closeViewPagerPosition()
                    createViewPager()
                }
                R.id.nav_tromso -> {
                   ValueHandler().setCityNumber("Tromsø")
                    closeViewPagerPosition()
                    createViewPager()
                }

                R.id.nav_om -> {
                    closeViewPager()
                    closeViewPagerPosition()
                    val nextFrag = FragmentOm()
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.fragment_holder, nextFrag)
                    transaction.commit()
                }
            }
            true
        }
    }


    //Overskriver onOptionsItemsSelected til å åpne menyen når bruker trykker
    //på meny-knappen i toolbaren. openDrawer() kalles for å åpne.
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    //Lager ViewPager og tabs til storbyene
    fun createViewPager() {
        val adapter = MyViewPagerAdapter(this.supportFragmentManager)

        adapter.addFragment(CityFragment(), "Nå")
        adapter.addFragment(ForecastFragment(), "Time for time")

        this.viewPager.adapter = adapter
        this.tabs.setupWithViewPager(this.viewPager)

        viewPager.visibility = View.VISIBLE
        tabs.visibility = View.VISIBLE
    }

    // Lager ViewPager og tabs til Min posisjon
    fun createViewPagerPosition() {
        val adapter = MyViewPagerAdapter(this.supportFragmentManager)

        adapter.addFragment(PositionFragment(), "Nå")
        adapter.addFragment(ForecastPositionFragment(), "Time for time")

        this.viewPagerPos.adapter = adapter
        this.tabsPos.setupWithViewPager(this.viewPagerPos)

        viewPagerPos.visibility = View.VISIBLE
        tabsPos.visibility = View.VISIBLE
    }

    // Lukker ViewPager til storbyene
    private fun closeViewPager(){
        viewPager.visibility = View.GONE
        tabs.visibility = View.GONE
    }

    // Lukker ViewPager til Min posisjon
    private fun closeViewPagerPosition(){
        viewPagerPos.visibility = View.GONE
        tabsPos.visibility = View.GONE
    }
}
