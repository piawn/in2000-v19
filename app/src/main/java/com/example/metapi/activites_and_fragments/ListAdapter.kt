package com.example.metapi.activites_and_fragments

//Kilde: https://codinginflow.com/tutorials/android/recyclerview-cardview/part-2-adapter

import android.content.Context
import android.support.v4.content.ContextCompat.getColor
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.metapi.R

class ListAdapter (var liste: MutableList<PrognoseElement>): RecyclerView.Adapter<ListAdapter.MinOversikt>() {

    lateinit var context : Context
    lateinit var cardView : CardView

    class MinOversikt(innhold: CardView): RecyclerView.ViewHolder(innhold){

        var klokkeslett = innhold.findViewById<TextView>(R.id.klokkeslett)
        var kvalitetsmelding = innhold.findViewById<TextView>(R.id.luftkvalitetsmelding)
    }

    override fun onCreateViewHolder(forelder: ViewGroup, type: Int): ListAdapter.MinOversikt {
        val tekst = LayoutInflater.from(forelder.context).inflate(R.layout.element, forelder, false) as CardView

        context = forelder.context
        cardView = tekst

        val viewHolder = MinOversikt(tekst)
        return viewHolder
    }


    // Setter riktig klokkeslett, luftkvalitetsmelding og farge på Elementene i CardViewet.
    override fun onBindViewHolder(holder: MinOversikt, position: Int){
        val tmp: PrognoseElement = liste[position]

        holder.klokkeslett.text = tmp.getKlokkeslett()
        holder.kvalitetsmelding.text = tmp.getKvalitetsmelding()
        if (tmp.getColor() == 0){
            println(holder.klokkeslett.text.toString())
            println("Color: " + tmp.getColor())
            val cardViewId = cardView.findViewById<CardView>(R.id.cardViewId)
            cardViewId.setBackgroundColor(getColor(context, R.color.colorTFTbackground))
        } else if (tmp.getColor() == 1){
            holder.kvalitetsmelding.setTextColor(getColor(context, R.color.colorLite))
        } else if (tmp.getColor() == 2){
            holder.kvalitetsmelding.setTextColor(getColor(context, R.color.colorModerat))
        } else if (tmp.getColor() == 3){
            holder.kvalitetsmelding.setTextColor(getColor(context, R.color.colorHoyt))
        } else if (tmp.getColor() == 4){
            holder.kvalitetsmelding.setTextColor(getColor(context, R.color.colorSvaertHoyt))
        }
    }

    override fun getItemCount() = liste.size

    override fun getItemViewType(position: Int): Int {
        return position
    }
}